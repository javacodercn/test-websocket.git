package cn.javacoder;

import cn.javacoder.engine.Action;
import cn.javacoder.engine.ActionResult;
import cn.javacoder.engine.ResultWrapper;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;

import java.lang.reflect.Type;
import java.util.List;

@Slf4j
public class ActionFrameHandler implements StompFrameHandler {
    private StompSession session;
    public ActionFrameHandler(StompSession session) {
        this.session = session;
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return Action.ActionList.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        log.info(JSON.toJSONString(payload));
        Action.ActionList list = (Action.ActionList)payload;
        Action action = list.get(0);
        ActionResult result = new ActionResult();
        result.setId(action.getId());
        result.setSuccess(true);
        session.send("/app/actionCallBack", result);
    }
}
