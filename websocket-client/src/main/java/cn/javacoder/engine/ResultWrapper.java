package cn.javacoder.engine;

import lombok.Data;

import java.util.List;

@Data
public class ResultWrapper {
    private List<Action> actions;
    private List<Result> results;

    public ResultWrapper(List<Action> actions, List<Result> results){
        this.actions = actions;
        this.results = results;
    }
}
