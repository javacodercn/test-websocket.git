package cn.javacoder.engine;

import lombok.Data;

/**
 * 动作执行结果
 */
@Data
public class ActionResult {
    private String id;
    private boolean success;
}
