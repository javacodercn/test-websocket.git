package cn.javacoder;

import cn.javacoder.engine.Result;
import cn.javacoder.engine.ResultWrapper;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;
import java.util.List;

@Slf4j
public class ConsultMessageFrameHandler implements StompFrameHandler {
    @Override
    public Type getPayloadType(StompHeaders headers) {
        return ResultWrapper.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        ResultWrapper wrapper = (ResultWrapper)payload;
        log.info(JSON.toJSONString(wrapper.getResults()));
    }
}
