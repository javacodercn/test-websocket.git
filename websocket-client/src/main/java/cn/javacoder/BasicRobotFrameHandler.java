package cn.javacoder;

import cn.javacoder.engine.ResultWrapper;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;

import java.lang.reflect.Type;

@Slf4j
public class BasicRobotFrameHandler implements StompFrameHandler {
    @Override
    public Type getPayloadType(StompHeaders headers) {
        return ResultWrapper.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        log.info(JSON.toJSONString(payload));
    }
}
