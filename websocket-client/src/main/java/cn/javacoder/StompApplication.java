package cn.javacoder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.converter.StringMessageConverter;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.UUID;

@Slf4j
public class StompApplication {
    public static final String TOPIC = "/queue/javacodercn";
    public static StompSession.Subscription subscription;
    public static void main(String[] args) throws IOException {

        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.initialize();

        ReactorNettyTcpStompClient client = new ReactorNettyTcpStompClient("192.168.140.128", 61613);
        client.setMessageConverter(new StringMessageConverter());
        client.setTaskScheduler(scheduler);
        StompHeaders connectHeaders = new StompHeaders();
        connectHeaders.setAcceptVersion("1.1");
        client.connect(connectHeaders, new StompSessionHandlerAdapter(){
            @Override
            public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
                log.error(exception.getMessage(), exception);
                throw new RuntimeException(exception);
            }

            @Override
            public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
                session.setAutoReceipt(true);
                log.error("--------22222---{}----------------");
                StompHeaders headers = new StompHeaders();
                headers.setDestination(TOPIC);
                headers.setReceiptId(UUID.randomUUID().toString());
                headers.setAck("client");
                headers.set("activemq.prefetchSize", Integer.toString(1));
                subscription = session.subscribe(headers, new MessageHandler(session) );
                super.afterConnected(session, connectedHeaders);
            }
        });
       System.in.read();
    }


    static class MessageHandler implements StompFrameHandler {
        StompSession session;
        public MessageHandler(StompSession session) {
            this.session = session;
        }

        @Override
        public Type getPayloadType(StompHeaders headers) {
            return String.class;
        }

        @Override
        public void handleFrame(StompHeaders headers, Object payload) {
            if(payload != null && payload.toString().contains("give up")) {
                StompHeaders giveupHeader = new StompHeaders();
                giveupHeader.set("JMSXGroupID", "xyz");
                giveupHeader.set("JMSXGroupSeq", "-1");
                giveupHeader.setDestination("javacodercn");
                session.send(giveupHeader, null);
            }
            log.error("-----33333---{}---{}----------------", headers.getMessageId(), payload);
            if(false) {
                throw new RuntimeException();
            }

            // headers immutable， 所以创建新的
            StompHeaders ackHeaders = new StompHeaders();
            ackHeaders.setMessageId(headers.getMessageId());
            ackHeaders.set("subscription", headers.getSubscription());
            subscription.getSubscriptionId();
            session.acknowledge(ackHeaders, true);
        }
    }

}
