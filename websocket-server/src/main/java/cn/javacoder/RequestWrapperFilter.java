package cn.javacoder;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

@Component
@Slf4j
public class RequestWrapperFilter implements Filter {
    @Override
    public void doFilter(ServletRequest _req, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) _req;
        Enumeration<String> enumeration = request.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String key = enumeration.nextElement();
            //log.info("key:{}, value:{}", key, request.getHeader(key));
        }
        MyServletRequestWrapper _request = new MyServletRequestWrapper(request);
        chain.doFilter(_request, response);
    }
}
