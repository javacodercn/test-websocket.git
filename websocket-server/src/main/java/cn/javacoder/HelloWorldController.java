package cn.javacoder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@Slf4j
public class HelloWorldController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    @Lazy
    private TrackingUserSessionDecorator trackingUserSessionService;

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public String sayHello(String user) {
        log.info(user);
        return "hi " + user;
    }

    @RequestMapping(value = "/pushToTopic", method = RequestMethod.GET)
    @ResponseBody
    public String pushToTopic(String content) {
        trackingUserSessionService.kickoffMonitor("fuckyou", "/123");
        simpMessagingTemplate.convertAndSend("/topic/greetings", content);
        return "pushed";
    }

    @RequestMapping(value = "/pushToUser", method = RequestMethod.GET)
    @ResponseBody
    public String pushToUser(String content, Principal principal) {
        log.info("content:{}, token:{}", content, principal.getName());
        simpMessagingTemplate.convertAndSendToUser("Rex","/queue/greetings", content);
        return "pushed";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public R login(@RequestBody User user) {
        if(StringUtils.hasText(user.getUsername()) && user.getUsername().equalsIgnoreCase(user.getPassword())) {
            long expire = System.currentTimeMillis() + 600*1000;
            String token = String.format("%s|%d", user.getUsername(), expire);
            return R.ok(token);
        }
        return R.fail("登录失败");
    }
}
