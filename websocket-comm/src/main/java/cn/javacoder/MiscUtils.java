package cn.javacoder;


import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class MiscUtils {

    public static String post(String url, String json ) throws Exception{
        String charset = "UTF-8";
        URLConnection connection = new URL(url).openConnection();
        connection.setDoOutput(true); // Triggers POST.
        connection.setRequestProperty("Accept-Charset", charset);
        connection.setRequestProperty("Content-Type", "application/json;charset=" + charset);

        try (OutputStream output = connection.getOutputStream()) {
            output.write(json.getBytes(charset));
        }

        InputStream response = connection.getInputStream();
        InputStreamReader reader = new InputStreamReader(response);
        StringBuffer sb = new StringBuffer();
        char[] buff = new char[1024];
        int sz = 0;
        while((sz = reader.read(buff, 0, 1024)) != -1){
            sb.append(new String(buff, 0, sz));
        }
        return sb.toString();
    }
}
